USE [QuanLyNhaHang]
GO
/****** Object:  Table [dbo].[Ban]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ban](
	[MaBan] [int] IDENTITY(1,1) NOT NULL,
	[TenBan] [nvarchar](50) NULL,
	[TinhTrang] [bit] NULL,
 CONSTRAINT [PK_Ban] PRIMARY KEY CLUSTERED 
(
	[MaBan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChiTietHoaDon]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietHoaDon](
	[MaChiTietHoaDon] [int] IDENTITY(1,1) NOT NULL,
	[MaSanPhan] [nvarchar](50) NULL,
	[SoLuongMua] [int] NULL,
	[MaHoaDon] [int] NULL,
 CONSTRAINT [PK_ChiTietHoaDon] PRIMARY KEY CLUSTERED 
(
	[MaChiTietHoaDon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDon](
	[MaHoaDon] [int] IDENTITY(1,1) NOT NULL,
	[NgayLap] [datetime] NULL,
	[ThoiDiemThanhToan] [datetime] NULL,
	[TinhTrang] [bit] NULL,
	[TongTien] [float] NULL,
	[MaBan] [int] NULL,
 CONSTRAINT [PK_HoaDon] PRIMARY KEY CLUSTERED 
(
	[MaHoaDon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Loai]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Loai](
	[MaLoai] [nvarchar](50) NOT NULL,
	[TenLoai] [nvarchar](50) NULL,
 CONSTRAINT [PK_Loai] PRIMARY KEY CLUSTERED 
(
	[MaLoai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhanVien](
	[MaNhanVien] [nvarchar](20) NOT NULL,
	[TenNhanVien] [nvarchar](50) NULL,
	[NgaySinh] [datetime] NULL,
	[DiaChi] [nvarchar](50) NULL,
	[username] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[Quyen] [bit] NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MaNhanVien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuanLyGioLam]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuanLyGioLam](
	[Id] [nvarchar](20) NOT NULL,
	[SoNgayLam] [int] NULL,
	[MaNhanVien] [nvarchar](20) NULL,
 CONSTRAINT [PK_QuanLyGioLam] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SanPham]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SanPham](
	[MaSanPham] [nvarchar](50) NOT NULL,
	[TenSanPham] [nvarchar](50) NULL,
	[SoLuong] [bigint] NULL,
	[Gia] [money] NULL,
	[Anh] [nvarchar](50) NULL,
	[NgayNhap] [datetime] NULL,
	[MaLoai] [nvarchar](50) NULL,
 CONSTRAINT [PK_SanPham] PRIMARY KEY CLUSTERED 
(
	[MaSanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ThongKe]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThongKe](
	[MaThongKe] [nvarchar](50) NOT NULL,
	[NgayLapThongKe] [datetime] NULL,
	[TongTien] [money] NULL,
 CONSTRAINT [PK_ThongKe] PRIMARY KEY CLUSTERED 
(
	[MaThongKe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Ban] ON 

INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (1, N'Bàn số 1', 0)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (2, N'Bàn số 2', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (3, N'Bàn số 3', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (4, N'Bàn số 4', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (5, N'Bàn số 5', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (18, N'Bàn số 6', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (19, N'Bàn số 7', 0)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (20, N'Bàn số 8', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (21, N'Bàn số 9', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (22, N'Bàn số 10', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (23, N'Bàn số 11', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (24, N'Bàn số 12', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (25, N'Bàn số 13', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (26, N'Bàn số 14', 0)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (27, N'Bàn số 15', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (28, N'Bàn số 16', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (29, N'Bàn số 17', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (30, N'Bàn số 18', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (31, N'Bàn số 19', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (32, N'Bàn số 20', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (33, N'Bàn số 21', 1)
INSERT [dbo].[Ban] ([MaBan], [TenBan], [TinhTrang]) VALUES (34, N'Bàn số 22', 1)
SET IDENTITY_INSERT [dbo].[Ban] OFF
SET IDENTITY_INSERT [dbo].[ChiTietHoaDon] ON 

INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (64, N'SP03', 2, 42)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (65, N'SP01', 2, 42)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (66, N'SP02', 1, 42)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (67, N'SP02', 1, 43)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (68, N'SP01', 3, 43)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (69, N'SP03', 1, 44)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (70, N'SP03', 1, 53)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (71, N'SP03', 1, 46)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (72, N'SP02', 3, 46)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (73, N'SP03', 3, 49)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (74, N'SP01', 3, 49)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (75, N'SP02', 3, 49)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (76, N'SP03', 1, 56)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (77, N'SP02', 3, 56)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (78, N'SP03', 1, 58)
INSERT [dbo].[ChiTietHoaDon] ([MaChiTietHoaDon], [MaSanPhan], [SoLuongMua], [MaHoaDon]) VALUES (79, N'SP01', 2, 58)
SET IDENTITY_INSERT [dbo].[ChiTietHoaDon] OFF
SET IDENTITY_INSERT [dbo].[HoaDon] ON 

INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (42, CAST(N'2019-04-30T22:42:48.940' AS DateTime), CAST(N'2019-04-30T22:42:57.487' AS DateTime), 1, 221000, 1)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (43, CAST(N'2019-04-30T22:43:01.977' AS DateTime), CAST(N'2019-04-30T22:43:09.340' AS DateTime), 1, 301000, 3)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (44, CAST(N'2019-05-05T10:44:56.847' AS DateTime), NULL, 0, NULL, 1)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (45, CAST(N'2019-05-05T10:46:42.253' AS DateTime), NULL, 0, NULL, 19)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (46, CAST(N'2019-05-25T15:05:32.140' AS DateTime), CAST(N'2019-05-25T15:05:43.467' AS DateTime), 1, 13000, 20)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (47, CAST(N'2019-05-25T15:05:51.620' AS DateTime), CAST(N'2019-05-25T17:14:16.760' AS DateTime), 1, 0, 20)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (48, CAST(N'2019-05-25T16:00:34.523' AS DateTime), CAST(N'2019-05-25T16:05:02.990' AS DateTime), 1, 0, 22)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (49, CAST(N'2019-05-25T16:01:27.510' AS DateTime), CAST(N'2019-05-25T16:04:59.390' AS DateTime), 1, 333000, 26)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (50, CAST(N'2019-05-25T16:09:42.993' AS DateTime), CAST(N'2019-05-25T17:14:19.583' AS DateTime), 1, 0, 24)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (51, CAST(N'2019-05-25T17:01:34.870' AS DateTime), CAST(N'2019-05-25T17:06:32.297' AS DateTime), 1, 0, 26)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (52, CAST(N'2019-05-25T17:03:32.877' AS DateTime), NULL, 0, 0, 27)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (53, CAST(N'2019-05-25T17:06:22.750' AS DateTime), NULL, 0, 0, 30)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (54, CAST(N'2019-05-25T17:14:23.937' AS DateTime), NULL, 0, NULL, 20)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (55, CAST(N'2019-05-25T17:15:00.453' AS DateTime), CAST(N'2019-05-25T17:23:56.300' AS DateTime), 1, 0, 21)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (56, CAST(N'2019-05-25T17:22:04.160' AS DateTime), NULL, 0, 0, 22)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (57, CAST(N'2019-05-25T17:24:02.560' AS DateTime), NULL, 0, NULL, 21)
INSERT [dbo].[HoaDon] ([MaHoaDon], [NgayLap], [ThoiDiemThanhToan], [TinhTrang], [TongTien], [MaBan]) VALUES (58, CAST(N'2019-05-25T17:24:15.580' AS DateTime), NULL, 0, 0, 26)
SET IDENTITY_INSERT [dbo].[HoaDon] OFF
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (N'NU', N'Nước uống')
INSERT [dbo].[Loai] ([MaLoai], [TenLoai]) VALUES (N'TA', N'Thức ăn')
INSERT [dbo].[NhanVien] ([MaNhanVien], [TenNhanVien], [NgaySinh], [DiaChi], [username], [password], [Quyen]) VALUES (N'nv01', N'Pham Van Tuan', CAST(N'2019-02-02T00:00:00.000' AS DateTime), N'Huế', N'tuan', N'123', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [TenSanPham], [SoLuong], [Gia], [Anh], [NgayNhap], [MaLoai]) VALUES (N'SP01', N'Gà', 1, 100000.0000, NULL, CAST(N'2018-02-02T00:00:00.000' AS DateTime), N'TA')
INSERT [dbo].[SanPham] ([MaSanPham], [TenSanPham], [SoLuong], [Gia], [Anh], [NgayNhap], [MaLoai]) VALUES (N'SP02', N'Rau', 2, 1000.0000, NULL, CAST(N'2019-01-02T00:00:00.000' AS DateTime), N'TA')
INSERT [dbo].[SanPham] ([MaSanPham], [TenSanPham], [SoLuong], [Gia], [Anh], [NgayNhap], [MaLoai]) VALUES (N'SP03', N'Nước ngọt', 5, 10000.0000, NULL, CAST(N'2018-03-02T00:00:00.000' AS DateTime), N'NU')
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_HoaDon] FOREIGN KEY([MaHoaDon])
REFERENCES [dbo].[HoaDon] ([MaHoaDon])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_HoaDon]
GO
ALTER TABLE [dbo].[ChiTietHoaDon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietHoaDon_SanPham1] FOREIGN KEY([MaSanPhan])
REFERENCES [dbo].[SanPham] ([MaSanPham])
GO
ALTER TABLE [dbo].[ChiTietHoaDon] CHECK CONSTRAINT [FK_ChiTietHoaDon_SanPham1]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_Ban] FOREIGN KEY([MaBan])
REFERENCES [dbo].[Ban] ([MaBan])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_Ban]
GO
ALTER TABLE [dbo].[QuanLyGioLam]  WITH CHECK ADD  CONSTRAINT [FK_QuanLyGioLam_QuanLyGioLam] FOREIGN KEY([MaNhanVien])
REFERENCES [dbo].[NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[QuanLyGioLam] CHECK CONSTRAINT [FK_QuanLyGioLam_QuanLyGioLam]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_Loai] FOREIGN KEY([MaLoai])
REFERENCES [dbo].[Loai] ([MaLoai])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_Loai]
GO
/****** Object:  StoredProcedure [dbo].[usp_insertBillInfo]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_insertBillInfo]
@idBill int,@idFood nvarchar(50),@soluong int
as
begin
	declare @isExitBillInfo int
	declare @foodCount int = 1

	select @isExitBillInfo = MaHoaDon,@foodCount = SoLuongMua from ChiTietHoaDon where MaHoaDon = @idBill and MaSanPhan = @idFood
	if(@isExitBillInfo > 0)
	begin
		declare @soluongmoi int  = @foodCount + @soluong
		if(@soluongmoi > 0 )
			update ChiTietHoaDon set SoLuongMua =  @foodCount+@soluong where MaSanPhan=@idFood
		else
			delete ChiTietHoaDon where MaHoaDon=@idBill and MaSanPhan = @idFood
	end
	else
	begin
		insert into ChiTietHoaDon values(@idFood,@soluong,@idBill)
	end 
end
GO
/****** Object:  StoredProcedure [dbo].[USP_SwitchTabel]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[USP_SwitchTabel]
@idTable1 INT, @idTable2 int
AS BEGIN

	DECLARE @idFirstBill int
	DECLARE @idSeconrdBill INT
	
	DECLARE @isFirstTablEmty INT = 1
	DECLARE @isSecondTablEmty INT = 1
	
	
	SELECT @idSeconrdBill = MaHoaDon FROM dbo.HoaDon WHERE MaBan = @idTable2 AND TinhTrang = 0
	SELECT @idFirstBill = MaHoaDon FROM dbo.HoaDon WHERE MaBan = @idTable1 AND TinhTrang = 0
	
	PRINT @idFirstBill
	PRINT @idSeconrdBill
	PRINT '-----------'
	
	IF (@idFirstBill IS NULL)
	BEGIN
		PRINT '0000001'
		INSERT dbo.HoaDon
		        ( NgayLap ,
		          MaBan ,
		          TinhTrang
		        )
		VALUES  ( GETDATE() , -- DateCheckIn - date
		          @idTable1 , -- idTable - int
		          0  -- status - int
		        )
		        
		SELECT @idFirstBill = MAX(MaHoaDon) FROM dbo.HoaDon WHERE MaBan = @idTable1 AND TinhTrang = 0
		
	END
	
	SELECT @isFirstTablEmty = COUNT(*) FROM dbo.ChiTietHoaDon WHERE MaHoaDon = @idFirstBill
	
	PRINT @idFirstBill
	PRINT @idSeconrdBill
	PRINT '-----------'
	
	IF (@idSeconrdBill IS NULL)
	BEGIN
		PRINT '0000002'
		INSERT dbo.HoaDon
		        ( 
		          NgayLap ,
		          MaBan ,
		          TinhTrang
		        )
		VALUES  ( GETDATE() , -- DateCheckIn - date
		          @idTable2 , -- idTable - int
		          0  -- status - int
		        )
		SELECT @idSeconrdBill = MAX(MaHoaDon) FROM dbo.HoaDon WHERE MaBan = @idTable2 AND TinhTrang = 0
		
	END
	
	SELECT @isSecondTablEmty = COUNT(*) FROM dbo.ChiTietHoaDon WHERE MaHoaDon = @idSeconrdBill
	
	PRINT @idFirstBill
	PRINT @idSeconrdBill
	PRINT '-----------'

	SELECT MaChiTietHoaDon INTO IDBillInfoTable FROM dbo.ChiTietHoaDon WHERE MaHoaDon = @idSeconrdBill
	
	UPDATE dbo.ChiTietHoaDon SET MaHoaDon = @idSeconrdBill WHERE MaHoaDon = @idFirstBill
	
	UPDATE dbo.ChiTietHoaDon SET MaHoaDon = @idFirstBill WHERE MaChiTietHoaDon IN (SELECT * FROM IDBillInfoTable)
	
	DROP TABLE IDBillInfoTable
	
	IF (@isFirstTablEmty = 0)
		UPDATE dbo.Ban SET TinhTrang = 0 WHERE MaBan = @idTable2
		
	IF (@isSecondTablEmty= 0)
		UPDATE dbo.Ban SET TinhTrang = 0 WHERE MaBan = @idTable1
END
GO
/****** Object:  StoredProcedure [dbo].[usp_switchTable]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_switchTable]
@idTable1 int,@idTable2 int
as
begin
	declare @idFirstBill int
	declare @idSecondBill int

	select @idSecondBill = MaHoaDon from HoaDon where MaBan = @idTable2 and TinhTrang = 0
	select @idFirstBill = MaHoaDon from HoaDon where MaBan = @idTable1 and TinhTrang = 0
	-- trường hợp idFirstBill Null
	if(@idFirstBill is null)
	begin
	select * from HoaDon
		insert into HoaDon values(GETDATE(),null,0,0,@idTable1)
		select @idFirstBill = Max(MaHoaDon) from HoaDon where MaBan = @idTable1 and TinhTrang = 0
		update Ban set TinhTrang=1 where MaBan=@idTable2
	end

	-- trường hợp idSecondBill Null
	if(@idSecondBill is null)
	begin
	select * from HoaDon
		insert into HoaDon values(GETDATE(),null,0,0,@idTable2)
		select @idSecondBill = Max(MaHoaDon) from HoaDon where MaBan = @idTable2 and TinhTrang = 0
		update Ban set TinhTrang=1 where MaBan=@idTable1
	end
	select MaChiTietHoaDon into idBillInfoTable from ChiTietHoaDon where  MaHoaDon = @idSecondBill

	update ChiTietHoaDon set MaHoaDon = @idSecondBill where MaHoaDon = @idFirstBill

	update ChiTietHoaDon set MaHoaDon = @idFirstBill where MaChiTietHoaDon in (select * from idBillInfoTable)

	drop table idBillInfoTable

end
GO
/****** Object:  StoredProcedure [dbo].[usp_ThongKe]    Script Date: 5/25/2019 05:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[usp_ThongKe]
@fromDate datetime,@toDate datetime
as
begin
	select b.TenBan,h.TongTien,h.NgayLap,h.ThoiDiemThanhToan 
	from HoaDon as h,Ban as b
	where NgayLap >= @fromDate and ThoiDiemThanhToan<=@toDate and h.TinhTrang=1
	and b.MaBan=h.MaBan
end
GO
