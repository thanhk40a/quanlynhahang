﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyNhaHang.bean;
using QuanLyNhaHang.dao;
using QuanLyNhaHang.model;

namespace QuanLyNhaHang.dao
{
    class MenuDao
    {
        private static MenuDao instance;
        public static MenuDao Instance
        {
            get { if (instance == null) instance = new MenuDao(); return instance; }
            private set { MenuDao.instance = value; }
        }
        private MenuDao() { }
        // lay list chitiethoadon theo id hoadon
        public List<Menu> getListChiTietHoaDon(int id)
        {
            dbDataContext db = Connect.Instance.Connection();
            List<Menu> listBillInfo = new List<Menu>();
            var ct = from c in db.ChiTietHoaDons
                     join d in db.HoaDons on c.MaHoaDon equals d.MaHoaDon
                     join s in db.SanPhams on c.MaSanPhan equals s.MaSanPham
                     where c.MaHoaDon == id && d.TinhTrang == false
                     select new Menu() {
                           TenSanPham = s.TenSanPham,
                           Gia = (float)s.Gia,
                           Soluong = (int)c.SoLuongMua,
                           TotalPrice = (float)s.Gia*(float)c.SoLuongMua
                     };
            if (ct.Count() != 0)
            {
                listBillInfo = ct.ToList();
            }
            return listBillInfo;
        }
    }
}
