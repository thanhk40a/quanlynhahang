﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyNhaHang.model;

namespace QuanLyNhaHang.dao
{
    class Connect
    {
        private static Connect instance;
        public static Connect Instance
        {
            get { if (instance == null) instance = new Connect(); return Connect.instance; }
            private set { Connect.instance = value; }
        }
        private Connect() { }
        // 
        public dbDataContext Connection()
        { 
            string stringConnect = @"Data Source=DESKTOP-PAQUVQ3\SQLEXPRESS;Initial Catalog=QuanLyNhaHang;Persist Security Info=True;User ID=sa;Password=123";
            dbDataContext db = new dbDataContext(stringConnect);
            return db;
        } 
    }
}
