﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyNhaHang.model;

namespace QuanLyNhaHang.dao
{
    class LoaiDao
    {
        private static LoaiDao instance;
        public static LoaiDao Instance
        {
            get { if (instance == null) instance = new LoaiDao();return instance; }
            private set { LoaiDao.instance = value; }
        }
        private LoaiDao() { }

        public List<Loai> getListLoai()
        {
            var l = from i in Connect.Instance.Connection().Loais
                    select i;
            List<Loai> listLoai = l.ToList();
            return listLoai;
        }


    }
}
