﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyNhaHang.model;

namespace QuanLyNhaHang.dao
{
    class ChiTietHoaDonDao
    {
        private static ChiTietHoaDonDao instance;
        public static ChiTietHoaDonDao Instance
        {
            get { if (instance == null) instance = new ChiTietHoaDonDao(); return instance; }
            private set { ChiTietHoaDonDao.instance = value; }
        }
        dbDataContext db = Connect.Instance.Connection();
        private ChiTietHoaDonDao() { }
        // lay list chitiethoadon theo id hoadon
        public List<ChiTietHoaDon> getListChiTietHoaDon(int id)
        {
            
            List<ChiTietHoaDon> listBillInfo = new List<ChiTietHoaDon>();
            var ct = from c in db.ChiTietHoaDons
                     join d in db.HoaDons on c.MaHoaDon equals d.MaHoaDon
                     join s in db.SanPhams on c.MaSanPhan equals s.MaSanPham
                     where c.MaHoaDon == id
                     select c;
            if (ct.Count() != 0)
            {
                listBillInfo = ct.ToList();
            }
            return listBillInfo;
        }
        public void insertChiTietHoaDon(string idFood,int soluong, int idHoaDon)
        {
            db.usp_insertBillInfo(idHoaDon, idFood, soluong);
        }
        //public Boolean switchTable(int id1,int id2)
        //{
        //    db.USP_SwitchTabel(id1, id2);
        //    return true;
        //}
    }
}
