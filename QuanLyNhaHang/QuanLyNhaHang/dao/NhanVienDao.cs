﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyNhaHang.model;


namespace QuanLyNhaHang.dao
{
    class NhanVienDao
    {
        private static NhanVienDao instance;

        public static NhanVienDao Instace
        {
            get { if (instance == null) instance = new NhanVienDao(); return instance; }
            private set { instance = value; }
        }
        private NhanVienDao() { }
        public Boolean checkLoginUser(string username, string password)
        {
            var u = from i in Connect.Instance.Connection().NhanViens
                    where (i.username.Equals(username)) && (i.password.Equals(password))
                    select i;
            if (u.Count() != 0)
                return true;
            return false;
        }
        public List<NhanVien> getListNhanVien()
        {
            return Connect.Instance.Connection().NhanViens.ToList();
        }
    }
}
