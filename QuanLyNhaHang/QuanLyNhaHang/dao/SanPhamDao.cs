﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyNhaHang.model;

namespace QuanLyNhaHang.dao
{
    class SanPhamDao
    {
        private static SanPhamDao instance;
        dbDataContext db = Connect.Instance.Connection();
        public static SanPhamDao Instance
        {
            get { if (instance == null) instance = new SanPhamDao();return instance; }
            private set { SanPhamDao.instance = value; }
        }
        private SanPhamDao() { }
        public List<SanPham> getListSanPhamByIdLoai(string id)
        {
            var s = from i in Connect.Instance.Connection().SanPhams
                    where i.MaLoai.Equals(id)
                    select i;
            List<SanPham> listSanPham = s.ToList();
            return listSanPham;
        }
        public List<SanPham> getListSanPham()
        {
            return Connect.Instance.Connection().SanPhams.ToList();
        }
        public Loai getLoaiByID(string id)
        {
            var u = from i in db.Loais
                    where i.MaLoai.Equals(id)
                    select i;
            return (Loai)u;

        }
    }
}
