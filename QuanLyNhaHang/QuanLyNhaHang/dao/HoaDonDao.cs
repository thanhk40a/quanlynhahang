﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyNhaHang.model;
using QuanLyNhaHang.bean;

namespace QuanLyNhaHang.dao
{
    class HoaDonDao
    {
        private static HoaDonDao instance;
        public static HoaDonDao Instance
        {
            get { if (instance == null) instance = new HoaDonDao(); return instance; }
            private set { HoaDonDao.instance = value; }
        }
        dbDataContext db = Connect.Instance.Connection();
        private HoaDonDao() { }
        public int getIdBill(int idTable)
        { 
            foreach(HoaDon item in Connect.Instance.Connection().HoaDons)
            {
                if (item.MaBan == idTable && item.TinhTrang==false)
                    return item.MaHoaDon;
            }
            return -1;
        }
        // them vao hoa don
        public void insertHoaDon(int idTable)
        {
            HoaDon hd = new HoaDon();
            hd.MaBan = idTable;
            hd.TinhTrang = false;
            hd.NgayLap = DateTime.Now;
            hd.ThoiDiemThanhToan = null;
            db.HoaDons.InsertOnSubmit(hd);
            db.SubmitChanges();
        }
        // lay max idbill
        public int maxIdBill()
        {
            var max = db.HoaDons.Max(x => x.MaHoaDon);
            return max;
        }
        // cap nhat tinh trang hoa don
        public void updateTinhTrang(int id,float totalPrice)
        {
            var s = from p in db.HoaDons
                    where p.MaHoaDon == id
                    select p;
            s.Single().TinhTrang = true;
            s.Single().ThoiDiemThanhToan = DateTime.Now;
            s.Single().TongTien = totalPrice;
            db.SubmitChanges();
        }
        public Object getBillListByDate(DateTime checkIn,DateTime checkOut)
        {
            var t = from h in db.HoaDons
                    join b in db.Bans on h.MaBan equals b.MaBan
                    where h.NgayLap >= checkIn && h.ThoiDiemThanhToan <=checkOut
                    select new
                    {
                        b.TenBan,
                        h.TongTien,
                        h.NgayLap,
                        h.ThoiDiemThanhToan
                    };
            Object listThongKe = db.usp_ThongKe(checkIn, checkOut);
            return listThongKe;
        }
    }
}
