﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyNhaHang.model;
using QuanLyNhaHang.dao;

namespace QuanLyNhaHang.dao
{
    class BanDao
    {
        private static BanDao instance;
        dbDataContext db = Connect.Instance.Connection();
        public static BanDao Instance
        {
            get { if (instance == null) instance = new BanDao(); return instance; }
            private set { BanDao.instance = value; }
        }
        public static int width = 100;
        public static int height = 100;
        private BanDao() { }
        
        public void switchTable(int id1,int id2)
        {
            db.usp_switchTable(id1, id2);
        }

        public List<Ban> loadTableList()
        {
            List<Ban> tableList = new List<Ban>();
            tableList = Connect.Instance.Connection().Bans.ToList();
            return tableList;
        }
    }
}
