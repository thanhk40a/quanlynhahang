﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhaHang.bean
{
    class ThongKe
    {
        private string tenBan;
        public string TenBan
        {
            get { return tenBan; }
            set { tenBan = value; }
        }
        private float tongTien;
        public float TongTien
        {
            get { return tongTien; }
            set { tongTien = value; }
        }
        private DateTime checkIn;
        public DateTime CheckIn
        {
            get { return checkIn; }
            set { checkIn = value; }
        }
        private DateTime checkOut;
        public DateTime CheckOut
        {
            get { return checkOut; }
            set { checkOut = value; }
        }
    }
}
