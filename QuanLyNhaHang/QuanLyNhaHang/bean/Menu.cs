﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyNhaHang.bean
{
    class Menu
    {
        private string tenSanPham;
        public string TenSanPham
        {
            get { return tenSanPham; }
            set { tenSanPham = value; }
        }
        private int soluong;
        public int Soluong
        {
            get { return soluong; }
            set { soluong = value; }
        }
        private float gia;
        public float Gia
        {
            get { return gia; }
            set { gia = value; }
        }
        private float totalPrice;

        public float TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; }
        }



    }
}
