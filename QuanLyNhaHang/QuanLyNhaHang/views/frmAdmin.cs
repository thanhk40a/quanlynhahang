﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyNhaHang.dao;

namespace QuanLyNhaHang.views
{
    public partial class frmAdmin : Form
    {
        public frmAdmin()
        {
            InitializeComponent();
            loadDatimePicker();
            loadListBillByDate(dtpkFromDate.Value, dtpkToDate.Value);
        }
        void loadDatimePicker()
        {
            DateTime today = DateTime.Now;
            dtpkFromDate.Value = new DateTime(today.Year, today.Month, 1);
            dtpkToDate.Value = dtpkFromDate.Value.AddMonths(2).AddDays(-1);
        }
        private void frmAdmin_Load(object sender, EventArgs e)
        {

        }
        void loadListBillByDate(DateTime checkIn,DateTime checkOut)
        {
            dtgvBill.DataSource = HoaDonDao.Instance.getBillListByDate(checkIn, checkOut);
        }
        private void btnThongKe_Click(object sender, EventArgs e)
        {
            loadListBillByDate(dtpkFromDate.Value, dtpkToDate.Value);
        }
    }
}
