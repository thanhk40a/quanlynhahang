﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyNhaHang.model;
using QuanLyNhaHang.dao;

namespace QuanLyNhaHang.views
{
    public partial class frmSanPham : Form
    {
        public frmSanPham()
        {
            InitializeComponent();
        }
        void loadListFood()
        {
            dtgvSanPham.DataSource = SanPhamDao.Instance.getListSanPham();

        }
        void addFoodBinding()
        {
            txtTen.DataBindings.Add(new Binding("text", dtgvSanPham.DataSource, "TenSanPham"));
            txtID.DataBindings.Add(new Binding("text", dtgvSanPham.DataSource, "MaSanPham"));
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void frmSanPham_Load(object sender, EventArgs e)
        {

        }
        void loadCategoryIntoCombobox(ComboBox cb)
        {
            cbDanhMuc.DataSource = LoaiDao.Instance.getListLoai();
            cbDanhMuc.DisplayMember = "TenLoai";
        }
        private void btnXem_Click(object sender, EventArgs e)
        {
            loadListFood();
            addFoodBinding();
            loadCategoryIntoCombobox(cbDanhMuc);
        }
    }
}
