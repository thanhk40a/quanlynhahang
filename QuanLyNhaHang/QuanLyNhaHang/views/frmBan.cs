﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyNhaHang.model;
using QuanLyNhaHang.dao;
using QuanLyNhaHang.bean;
using Menu = QuanLyNhaHang.bean.Menu;
using System.Globalization;

namespace QuanLyNhaHang.views
{
    public partial class frmBan : Form
    {
        public frmBan()
        {
            InitializeComponent();
            loadTable();
            loadCategory();
            loadComboboxTable(cbSwitchTable);
        }
        void loadCategory()
        {
            List<Loai> listLoai = LoaiDao.Instance.getListLoai();
            cbCategory.DataSource = listLoai;
            cbCategory.DisplayMember = "TenLoai";
        }
        void loadFoodByCategoty(string id)
        {
            List<SanPham> listSanPham = SanPhamDao.Instance.getListSanPhamByIdLoai(id);
            cbFood.DataSource = listSanPham;
            cbFood.DisplayMember = "TenSanPham";
        }
        void loadTable()
        {
            flpTable.Controls.Clear();
            List<Ban> listTable = BanDao.Instance.loadTableList();
            foreach(Ban item in listTable)
            {
                Button btn = new Button()
                {
                    Width = BanDao.width,
                    Height = BanDao.height
                };
                btn.Text = item.TenBan + Environment.NewLine + (item.TinhTrang==true?"trống":"Có người");
                btn.Click += btn_Click ;
                btn.Tag = item;
                switch (item.TinhTrang)
                {
                    case  true:
                        btn.BackColor = Color.Aqua;
                        break;
                    default:
                        btn.BackColor = Color.OrangeRed;
                        break;
                }
                flpTable.Controls.Add(btn);
            }
        }
        void loadComboboxTable(ComboBox cb)
        {
            cb.DataSource = BanDao.Instance.loadTableList();
            cb.DisplayMember = "TenBan";
        }
        void showBill(int id)
        {
            lvBill.Items.Clear();
            float total = 0;
            List<ChiTietHoaDon> listBillInfo = ChiTietHoaDonDao.Instance.getListChiTietHoaDon(HoaDonDao.Instance.getIdBill(id));
            List<Menu> list = MenuDao.Instance.getListChiTietHoaDon(HoaDonDao.Instance.getIdBill(id));
            foreach (Menu item in list)
            {
                ListViewItem lsvitem = new ListViewItem(item.TenSanPham.ToString());
                lsvitem.SubItems.Add(item.Soluong.ToString());
                lsvitem.SubItems.Add(item.Gia.ToString());
                lsvitem.SubItems.Add(item.TotalPrice.ToString());
                lvBill.Items.Add(lsvitem);
                total += item.TotalPrice;
            }
            CultureInfo culture = new CultureInfo("vi-VN");
            txtTotalPrice.Text = total.ToString("c",culture);
        }
 

        #region Event
        private void btn_Click(object sender, EventArgs e)
        {
            int tableId = ((sender as Button).Tag as Ban).MaBan;
            lvBill.Tag = (sender as Button).Tag;
            showBill(tableId);
        }
        #endregion
        private void nhânViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNhanVien f = new frmNhanVien();
            this.Hide();
            f.ShowDialog();
            this.Show();
        }

        private void cbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = "";
            ComboBox comboBox = sender as ComboBox;
            if (comboBox.SelectedItem == null)
                return;
            Loai seleted = comboBox.SelectedItem as Loai;
            id = seleted.MaLoai;
            loadFoodByCategoty(id);
        }

        private void btnAddFood_Click(object sender, EventArgs e)
        {
            Ban ban = lvBill.Tag as Ban;
            int idHoaDon = HoaDonDao.Instance.getIdBill(ban.MaBan);
            string foodid = (cbFood.SelectedItem as SanPham).MaSanPham;
            int soluong = (int)nudSoLuong.Value;

            if (idHoaDon == -1)
            {
                HoaDonDao.Instance.insertHoaDon(ban.MaBan);
                ChiTietHoaDonDao.Instance.insertChiTietHoaDon(foodid, soluong, HoaDonDao.Instance.maxIdBill());
            }
            else
            {
                ChiTietHoaDonDao.Instance.insertChiTietHoaDon(foodid, soluong, idHoaDon);
            }
            showBill(ban.MaBan);
            loadTable();
        }

        private void btnCheckout_Click(object sender, EventArgs e)
        {
            Ban ban = lvBill.Tag as Ban;
            string st = txtTotalPrice.Text.Split(',')[0].Replace(".", "");
            double total = Convert.ToDouble(st);
            int idBill = HoaDonDao.Instance.getIdBill(ban.MaBan);
            if(idBill != -1)
            {
                if(MessageBox.Show("Bạn có chắc muốn thanh toán cho bàn "+ ban.TenBan,"thông báo", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                    HoaDonDao.Instance.updateTinhTrang(idBill, (float)total);
                    showBill(ban.MaBan);
                    loadTable();
                }
            }
        }

        private void btnSwitchTable_Click(object sender, EventArgs e)
        {
            int id1 = (lvBill.Tag as Ban).MaBan;
            int id2 = (cbSwitchTable.SelectedItem as Ban).MaBan;
            if(MessageBox.Show(string.Format("Bạn có muốn chuyển bàn {0} qua bàn {1}",id1,id2),"thông báo", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK){
                BanDao.Instance.switchTable(id1, id2);
                loadTable();
            }
        }

        private void adminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAdmin f = new frmAdmin();
            this.Hide();
            f.ShowDialog();
            this.Show();
        }


        private void sảnPhẩmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSanPham f = new frmSanPham();
            f.Show();
        }

        private void frmBan_Load(object sender, EventArgs e)
        {

        }
    }
}
